import matplotlib.pyplot as plt
import numpy
import pandas
import sympy

# 导入基本数学变量和函数
from math import (
    ceil,
    copysign,
    degrees,
    radians,
    log,
    log2,
    log10,
    pow,
    exp,
    prod,
    sqrt,
    sin,
    cos,
    tan,
    fabs,
    factorial,
    floor,
    fmod,
    gcd,
    fsum,
    hypot,
    isfinite,
    isinf,
    isnan,
    modf,
    e,
    pi,
    trunc,
    expm1,
    frexp,
    ldexp
)

# 导入numpy 统计函数
from numpy import (
    sum,
    mean,
    average,
    std,
    var,
    min,
    max,
    median
)

# 导入numpy 随机数
from numpy.random import (
    rand,
    randn,
    randint,
    random_integers,
    random_sample,
    random,
    ranf,
    sample,
    choice,
    seed
)

from .pyminer_algorithms.linear_algebra import *

# 导入数据读取相关模块
from pandas.io.api import (
    # excel
    ExcelFile,
    ExcelWriter,
    read_excel,
    # parsers
    read_csv,
    read_fwf,
    read_table,
    # pickle
    read_pickle,
    to_pickle,
    # pytables
    HDFStore,
    read_hdf,
    # sql
    read_sql,
    read_sql_query,
    read_sql_table,
    # misc
    read_clipboard,
    read_parquet,
    read_orc,
    read_feather,
    read_gbq,
    read_html,
    read_json,
    read_stata,
    read_sas,
    read_spss,
)

# 导入模型相关模块
from sklearn.model_selection import train_test_split  # 将数据分为测试集和训练集

# 分类 Classification
# from sklearn import SomeClassifier
# from sklearn.linear_model import SomeClassifier
# from sklearn.ensemble import SomeClassifier

# 回归 Regression
# from sklearn import SomeRegressor
# from sklearn.linear_model import SomeRegressor
# from sklearn.ensemble import SomeRegressor

# 聚类 Clustering
# from sklearn.cluster import SomeModel

# 降维 Dimensionality Reduction
# from sklearn.decomposition import SomeModel

# 模型选择 Model Selection
# from sklearn.model_selection import SomeModel


# 预处理 Preprocessing
# from sklearn.preprocessing import SomeModel

from sklearn.linear_model import LinearRegression  # 引入线性回归模型
from sklearn.model_selection import cross_val_score  # 交叉验证
from sklearn.neighbors import KNeighborsClassifier


def plot(x, y) -> None:
    """
    提供类似MATLAB的方式进行绘图

    参数
    ----------
    x : np.array
    y : np.array
    """
    plt.plot(x, y)
    plt.show()


def version():
    print("1.0.1")


def size(x):
    """
    获取矩阵的行数和列数

    参数
    ----------
    x : numpy.ndarray, pandas.DataFrame, sympy.Matrix

    注意
    ----------
    （1）返回长度时，仅针对最外层，不做深度统计
    """
    sizeFunc = [
    	numpy.ndarray, pandas.DataFrame, sympy.Matrix
    ]
    try:
    	res = len(x) if type(x) not in sizeFunc else x.shape
    	return(res)
    except:
    	e = str(type(x)).split("'")[1]
    	raise TypeError("'{}' object does not support checking size!".format(e)
