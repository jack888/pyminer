# `linear_space`函数说明

## 用法

`linear_space(start, stop, count)`

## 说明

在`start`和`stop`之间（包括两端点）生成`count`个点位。

## 位置参数

1. `start`：起点处的位置，可以是一个标量或矩阵。
1. `stop`：终点处的位置，可以是一个标量或矩阵。
1. `count`：点位数量，包括起点和终点。

## 返回值

比`start`和`stop`高一维的矩阵。

# 参考文献

1. [`linspace`帮助文档. Numpy.][np]
1. [`linspace`帮助文档. MATLAB.][ml]

[np]: https://numpy.org/doc/stable/reference/generated/numpy.linspace.html
[ml]: https://ww2.mathworks.cn/help/matlab/ref/linspace.html