import numpy


def matrix_transpose(array: numpy.ndarray) -> numpy.ndarray:
    """
    矩阵的转置
    :param array: 待转置的矩阵
    :return: 转置后的矩阵
    """
    assert len(array.shape) in (1, 2), '转置操作仅支持一维向量和二维矩阵'
    if len(array.shape) == 1:
        array = numpy.atleast_2d(array)
        return numpy.transpose(array)
    if len(array.shape) == 2:
        return numpy.transpose(array)
