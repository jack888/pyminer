import numpy
from . import utils


def ones(shape, *shapes, type=float, dtype=None) -> numpy.ndarray:
    shape = utils.preprocess_shape(shape, *shapes)
    type = utils.preprocess_type(type, dtype)
    return numpy.ones(shape, dtype=type)
