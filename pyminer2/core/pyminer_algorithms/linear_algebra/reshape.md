# `reshape`函数说明

## 用法

`reshape(arr, shape)`

## 说明

改变矩阵的形状。

## 位置参数

1. `arr`：原始矩阵
1. `shape`：新的矩阵的形状

## 返回值

形状改变后的矩阵。

## 备注

本函数并不是`numpy.reshape`，而是对其的封装。
本函数并不支持`numpy.reshape`的一系列高级操作，
例如基于内存的顺序等。
如有需要请直接使用`numpy.reshape`。

`MATLAB`的`reshape`函数支持`shape`和`*shape`两种方式。
由于时间考虑现仅支持`shape`的参数调用方式。
如您有时间欢迎补全。

# 参考文献

1. [`reshape`帮助文档. Numpy.][numpy]
1. [`reshape`帮助文档. MATLAB.][matlab]

[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.reshape.html
[matlab]: https://ww2.mathworks.cn/help/matlab/ref/reshape.html
