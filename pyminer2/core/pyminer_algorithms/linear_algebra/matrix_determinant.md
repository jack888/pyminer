# `matrix_determinant`函数说明

## 用法

`matrix_determinant(arr)`

## 说明

计算矩阵的行列式。

## 位置参数

1. `arr`：待计算的矩阵。

## 返回值

矩阵的行列式。

# 参考文献

1. [`linalg.det`帮助文档. Numpy.][np]
1. [`det`帮助文档. MATLAB][ml]

[np]: https://numpy.org/doc/stable/reference/generated/numpy.linalg.det.html
[ml]: https://ww2.mathworks.cn/help/matlab/ref/det.html