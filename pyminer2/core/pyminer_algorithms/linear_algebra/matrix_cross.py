import numpy


def matrix_cross(a: numpy.ndarray, b: numpy.ndarray) -> numpy.ndarray:
    """
    计算两个矩阵的叉积
    :param a: 第一个矩阵
    :param b: 第二个矩阵
    :return:
    """
    assert isinstance(a, numpy.ndarray), f'parameter a should be an array, not "{type(a)}"'
    assert isinstance(b, numpy.ndarray), f'parameter b should be an array, not "{type(b)}"'
    assert a.shape[-1] in (2, 3), f'last dimension of array a should be 2 or 3, not "{a.shape[-1]}"'
    assert b.shape[-1] in (2, 3), f'last dimension of array b should be 2 or 3, not "{b.shape[-1]}"'
    assert a.shape == b.shape, f'two array should have same shape, not "{a.shape}" & "{b.shape}"'
    return numpy.cross(a, b)
