# `matrix_inverse`函数说明

## 用法

`matrix_inverse(arr)`

## 说明

对矩阵求逆。

矩阵应当是二维方阵。

## 位置参数

1. `arr`：待求逆的方阵。

## 返回值

矩阵的逆矩阵。

## 备注

这个函数直接调用的`numpy.linalg.inv`函数，
不过做了一些限制，比如仅支持二维矩阵。

# 参考文献

1. [`inv`帮助文档. Numpy.][numpy]
1. [`inv`帮助文档. MATLAB.][matlab]

[matlab]: https://ww2.mathworks.cn/help/matlab/ref/inv.html
[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.linalg.inv.html
