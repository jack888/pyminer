import numpy


def shape(arr: numpy.ndarray):
    assert isinstance(arr, numpy.ndarray), 'only `numpy.ndarray` supported'
    return arr.shape
