# `ones`函数说明

## 用法

`ones(shape, **kwargs)`

`ones(*shape, **kwargs)`

## 说明

返回一个新的全是`1`的矩阵，矩阵的维度和大小通过给定的参数进行指定。

## 位置参数

1. `shape`：矩阵的形状，是整数的数组，整数的个数表示维度，整数的大小表示该维度的大小。
    这个参数可以加括号放在一起，如 `ones((3,4))`，
    也可以不加括号，如`ones(3,4)`，两种写法等价，建议加括号。

## 关键字参数

1. `type=float`: 矩阵的类型，可以是一个`python`类型或者`numpy`类型，
包括`int`、`float`等。`type`也可以写为`dtype`，以满足`numpy`用户的习惯。
具体支持的类型请参照[numpy的类型][numpy的类型]。

## 返回值

按照指定的维度、指定的大小、指定的类型生成的全为`1`的多维矩阵。

[numpy的类型]: https://numpy.org/devdocs/user/basics.types.html

# 参考文献

1. [`ones`帮助文档. MATLAB.][matlab]
1. [`ones`帮助文档. Numpy.][numpy]
1. [`ones`帮助文档. R.][R]

[matlab]: https://ww2.mathworks.cn/help/matlab/ref/ones.html
[numpy]: https://numpy.org/doc/stable/reference/generated/numpy.ones.html
[R]: https://www.rdocumentation.org/packages/matlab/versions/1.0.2/topics/ones