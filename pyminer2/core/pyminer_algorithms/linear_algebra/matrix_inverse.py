import re

import numpy

from .exceptions import LinearAlgebraError


def matrix_inverse(arr: numpy.ndarray) -> numpy.ndarray:
    assert isinstance(arr, numpy.ndarray), 'only `numpy.ndarray` supported'
    assert len(arr.shape) == 2, 'only 2d array supported'
    try:
        return numpy.linalg.inv(arr)
    except numpy.linalg.LinAlgError as error:
        if re.match('Singular matrix', error.args[0]):
            raise LinearAlgebraError('inverse of singular matrix is invalid')
        if re.match(r'Last 2 dimensions of the array must be square', error.args[0]):
            raise LinearAlgebraError('inverse of non-square matrix is invalid')
        raise
