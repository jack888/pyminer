import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

# 导入基本数学变量和函数
from math import (
    ceil,
    copysign,
    degrees,
    radians,
    log,
    log2,
    log10,
    pow,
    exp,
    prod,
    sqrt,
    sin,
    cos,
    tan,
    fabs,
    factorial,
    floor,
    fmod,
    gcd,
    fsum,
    hypot,
    isfinite,
    isinf,
    isnan,
    modf,
    e,
    pi,
    trunc,
    expm1,
    frexp,
    ldexp
)

# 导入numpy 统计函数
from numpy import (
    sum,
    mean,
    average,
    std,
    var,
    min,
    max,
    median
)

# 导入numpy 随机数
from numpy.random import (
    rand,
    randn,
    randint,
    random_integers,
    random_sample,
    random,
    ranf,
    sample,
    choice,
    seed
)

from .pyminer_algorithms.linear_algebra import *

# 导入数据读取相关模块
from pandas.io.api import (
    # excel
    ExcelFile,
    ExcelWriter,
    read_excel,
    # parsers
    read_csv,
    read_fwf,
    read_table,
    # pickle
    read_pickle,
    to_pickle,
    # pytables
    HDFStore,
    read_hdf,
    # sql
    read_sql,
    read_sql_query,
    read_sql_table,
    # misc
    read_clipboard,
    read_parquet,
    read_orc,
    read_feather,
    read_gbq,
    read_html,
    read_json,
    read_stata,
    read_sas,
    read_spss,
)

# 导入模型相关模块
from sklearn.model_selection import train_test_split  # 将数据分为测试集和训练集

# 分类 Classification
# from sklearn import SomeClassifier
# from sklearn.linear_model import SomeClassifier
# from sklearn.ensemble import SomeClassifier

# 回归 Regression
# from sklearn import SomeRegressor
# from sklearn.linear_model import SomeRegressor
# from sklearn.ensemble import SomeRegressor

# 聚类 Clustering
# from sklearn.cluster import SomeModel

# 降维 Dimensionality Reduction
# from sklearn.decomposition import SomeModel

# 模型选择 Model Selection
# from sklearn.model_selection import SomeModel


# 预处理 Preprocessing
# from sklearn.preprocessing import SomeModel

from sklearn.linear_model import LinearRegression  # 引入线性回归模型
from sklearn.model_selection import cross_val_score  # 交叉验证
from sklearn.neighbors import KNeighborsClassifier


def plot(x, y) -> None:
    """
    提供类似MATLAB的方式进行绘图

    参数
    ----------
    x : np.array
    y : np.array
    """
    plt.plot(x, y)
    plt.show()


def version():
    print("1.0.1")


def size(x) -> np.array:
    """
    获取矩阵的行数和列数

    参数
    ----------
    x : np.array
    """
    if type(x) is np.ndarray:
        return x.shape
    else:
        try:
            return np.array(x).shape
        except:
            return "data type is not support"
