from PyQt5.QtCore import QLineF, QPointF, Qt, QRectF, pyqtSignal
from PyQt5.QtGui import QPolygonF, QPen, QPainterPath, QColor, QPainter, QBrush
from PyQt5.QtWidgets import QGraphicsLineItem, QGraphicsItem, QGraphicsSceneMouseEvent, QGraphicsObject, \
    QStyleOptionGraphicsItem, QWidget, QGraphicsSceneHoverEvent

from typing import TYPE_CHECKING, Tuple, List, Callable

if TYPE_CHECKING:
    from .flowchart_widget import Node, PMGraphicsScene

COLOR_NORMAL = QColor(212, 227, 242)
COLOR_HOVER = QColor(255, 200, 00)
COLOR_HOVER_PORT = QColor(0, 0, 50)
COLOR_HOVER_MID_POINT = QColor(0, 0, 200)
COLOR_SELECTED = QColor(255, 255, 0)


def round_position(point: QPointF, pixels=5):
    x, y = point.x(), point.y()
    x_cor, y_cor = round(x * 1.0 / pixels) * pixels, round(y * 1.0 / pixels) * pixels
    return QPointF(x_cor, y_cor)


class PMGGraphicsLineItem(QGraphicsLineItem):
    def __init__(self, line: QLineF):
        super(PMGGraphicsLineItem, self).__init__(line)
        self.callback = None
        self.parent_line: 'CustomLine' = None

    def bind_callback(self, parent_line: 'CustomLine' = None):
        self.parent_line = parent_line

    def mousePressEvent(self, event: 'QGraphicsSceneMouseEvent') -> None:
        """
        这里不能进行继承。一旦进行了继承，似乎会让下面所有的东西都收到信号，从而无法选定。
        :param event:
        :return:
        """
        super(PMGGraphicsLineItem, self).mousePressEvent(event)
        self.parent_line.on_line_item_pressed(event, self)

    def mouseDoubleClickEvent(self, event: 'QGraphicsSceneMouseEvent') -> None:
        self.parent_line.add_mid_point(self, pos=event.scenePos())

    def bind_mouse_clicked(self, callback: Callable):
        self.callback = callback


class CustomLine(QGraphicsLineItem):
    """
    CustomLine不是QObject，没有办法绑定信号，只能用回调函数的方式。
    repaint_callback是重新绘制时触发的事件。重新绘制时应当进行全画布的刷新。
    这是一个line，其中起点是start_port,终点end_port，中继点为mid_points中从第1个元素开始依次向前。
    所以一共有起点+终点+len(mid_points)个节点，从而有len(mid_points)+1个线段。

    每一个线段背后都是一个QGraphicsLineItem。为何不是简单绘制？盖因一般的绘制没有办法捕获鼠标事件。
    如果使用QGraphicsLineItem，那么可以捕获键盘事件。当拖动节点的时候，每一个QGraphicsLineItem都会调用setLine方法，
    刷新线段位置。

    当点击线段的时候会触发选择事件。首先，线段会调用QGraphicsScene（self.canvas）进行一个清除选择的操作————如果不按ctrl的话。
    然后设置本身状态为选择。

    self.canvas.selectedItems()可获取当前被选中的部件。

    下一步任务：插入节点、删除节点。
    """
    repaint_callback = None

    def __init__(self, start_port: 'CustomPort', end_port: 'CustomPort', canvas: 'PMGraphicsScene' = None,
                 mid_points: 'List[CustomMidPoint]' = None):
        super(CustomLine, self).__init__()
        self.color = COLOR_NORMAL
        self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)  # 拖动
        self.start_port = start_port
        self.end_port = end_port
        if self not in start_port.connected_lines:
            start_port.connected_lines.append(self)
        if self not in end_port.connected_lines:
            end_port.connected_lines.append(self)
        self.line_Items: List['QGraphicsLineItem'] = []
        self.center_points = mid_points if (mid_points is not None) else []
        for p in self.center_points:
            canvas.addItem(p)
            p.point_dragged.connect(self.refresh)
            p.line = self
        canvas.signal_clear_selection.connect(self.on_clear_selection)
        self.refresh()
        self.canvas = canvas
        self.init_line_items()

    def remove_mid_point(self, mid_point: 'CustomMidPoint'):
        index = self.center_points.index(mid_point)
        point_to_remove = self.center_points.pop(index)
        line_to_remove = self.line_Items.pop(index)
        self.canvas.removeItem(point_to_remove)
        self.canvas.removeItem(line_to_remove)
        self.canvas.removeItem(mid_point)
        self.refresh_line_items()
        self.update()
        self.canvas.graphics_view.viewport().update()

    def add_mid_point(self, line_item: 'PMGGraphicsLineItem', pos: QPointF):
        index = self.line_Items.index(line_item)
        new_center_point = CustomMidPoint(pos, line=self)
        self.canvas.addItem(new_center_point)
        self.center_points.insert(index, new_center_point)
        new_center_point.point_dragged.connect(self.refresh)
        if index == 0:
            last_pos = self.start_port.center_pos
        else:
            last_pos = self.center_points[index].center_pos
        next_pos = new_center_point.center_pos
        new_line_item = PMGGraphicsLineItem(QLineF(last_pos, next_pos))
        new_line_item.bind_callback(self)
        pen = QPen()
        pen.setColor(self.color)
        pen.setWidth(5)
        new_line_item.setPen(pen)
        self.canvas.addItem(new_line_item)
        self.line_Items.insert(index, new_line_item)
        self.refresh_line_items()
        self.update()
        self.canvas.graphics_view.viewport().update()

    def init_line_items(self):
        """
        初始化线段，包括其中间节点。
        但是目前不够完善，当只有一节的时候，没有办法完成。
        :return:
        """
        last_pos = self.start_port.center_pos
        for p in self.center_points:
            pos = p.center_pos
            line_item = PMGGraphicsLineItem(QLineF(last_pos, pos))
            # line_item.mousePressEvent = self.on_line_item_pressed
            line_item.bind_callback(self)
            pen = QPen()
            pen.setColor(self.color)
            pen.setWidth(5)
            line_item.setPen(pen)
            self.canvas.addItem(line_item)
            last_pos = pos
            self.line_Items.append(line_item)
        line_item = PMGGraphicsLineItem(QLineF(last_pos, self.end_port.center_pos))
        # line_item.mousePressEvent = self.on_line_item_pressed
        line_item.bind_callback(self)
        self.canvas.addItem(line_item)
        self.line_Items.append(line_item)

    def refresh_line_items(self):
        """
        刷新背后的直线段。
        :return:
        """
        last_pos = self.start_port.center_pos
        if len(self.line_Items) == 0:
            return
        for i, p in enumerate(self.center_points):
            pos = p.center_pos
            line = QLineF(last_pos, pos)
            self.line_Items[i].setLine(line)
            last_pos = pos

        line = QLineF(last_pos, self.end_port.center_pos)
        self.line_Items[-1].setLine(line)

    def on_line_item_pressed(self, e: 'QGraphicsSceneMouseEvent', line_item: 'CustomLine'):
        """
        当线段被点击时触发的事件。
        :param e:
        :return:
        """
        print(line_item)
        if e.button() == Qt.LeftButton:
            if not e.modifiers() == Qt.ControlModifier:
                self.canvas.signal_clear_selection.emit()
            self.canvas.select_item(self)
            print(self.canvas.selected_items)
            self.color = COLOR_SELECTED

    def on_clear_selection(self):
        """
        当清除选择时触发的事件。
        :return:
        """
        self.color = COLOR_NORMAL
        self.canvas.unselect_item(self)
        # self.update()

    def refresh(self):
        """
        当刷新时触发的事件
        :return:
        """
        self.refresh_line_items()
        self.update()

    def draw_arrow(self, QPainter, point_1: QPointF, point_2: QPointF) -> 'QPolygonF':
        """
        绘制箭头。
        :param QPainter:
        :param point_1:
        :param point_2:
        :return:
        """

        line = QLineF(point_1, point_2)
        v = line.unitVector()

        v.setLength(20)  # 改变单位向量的大小，实际就是改变箭头长度
        v.translate(QPointF(int(line.dx() / 2), int(line.dy() / 2)))

        n = v.normalVector()  # 法向量
        n.setLength(n.length() * 0.2)  # 这里设定箭头的宽度
        n2 = n.normalVector().normalVector()  # 两次法向量运算以后，就得到一个反向的法向量
        p1 = v.p2()
        p2 = n.p2()
        p3 = n2.p2()
        QPainter.drawPolygon(p1, p2, p3)
        return QPolygonF([p1, p2, p3, p1])

    def paint(self, q_painter: 'QPainter', style_option_graphics_item: 'QStyleOptionGraphicsItem',
              widget: 'QWidget' = None):

        pen = QPen()
        pen.setColor(self.color)
        pen.setWidth(3)
        pen.setJoinStyle(Qt.MiterJoin)  # 让箭头变尖
        q_painter.setPen(pen)

        path = QPainterPath()

        point1 = self.start_port
        path.moveTo(self.start_port.center_pos)
        for p in self.center_points + [self.end_port]:
            q_painter.drawLine(QLineF(point1.center_pos, p.center_pos))
            arrow = self.draw_arrow(q_painter, point1.center_pos, p.center_pos)
            path.addPolygon(arrow)
            point1 = p

        if self.repaint_callback is not None:
            self.repaint_callback()

    def get_central_points_positions(self):
        """
        获取所有中间节点的位置。
        :return:
        """
        positions = []
        for point in self.center_points:
            positions.append((point.x(), point.y()))
        return positions

    def hoverEnterEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        """
        悬浮事件进入所触发的事件。
        :param event:
        :return:
        """
        self.color = COLOR_HOVER_PORT

        self.update()

    def on_delete(self):
        """
        删除线对象
        首先从起始和结束的端口的连线中删除这个线；
        然后，从画布上移除自身所有的中间线段对象和中继点对象
        从画布上删除自身；
        从所有连线的列表中移除自身。
        :return:
        """
        try:
            self.start_port.connected_lines.remove(self)
        except ValueError:
            pass
        try:
            self.end_port.connected_lines.remove(self)
        except ValueError:
            pass
        for line_item in self.line_Items:
            self.canvas.removeItem(line_item)
        for mid_item in self.center_points:
            self.canvas.removeItem(mid_item)
        self.canvas.removeItem(self)
        self.canvas.lines.remove(self)


class CustomMidPoint(QGraphicsObject):
    point_dragged = pyqtSignal(QGraphicsObject)

    def __init__(self, pos: QPointF = None, line: 'CustomLine' = None):
        super(CustomMidPoint, self).__init__()
        # self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)  # 拖动
        self.setAcceptHoverEvents(True)  # 接受鼠标悬停事件
        self.relative_pos = (0, 0)
        self.color = COLOR_NORMAL
        self.size = (10, 10)
        # self.line = line
        if pos is not None:
            # self.setPos()
            # if isinstance(pos,list) or isinstance(pos,tuple):
            #     self.setPos(pos[0],pos[1])
            # else:
            self.setPos(pos)
        self.line = line

    def boundingRect(self):
        return QRectF(0, 0, self.size[0], self.size[1])

    @property
    def center_pos(self):
        return QPointF(self.x() + self.size[0] / 2, self.y() + self.size[1] / 2)

    def mouseMoveEvent(self, event: 'QGraphicsSceneMouseEvent') -> None:
        """
        鼠标拖动时触发的事件。
        :param event:
        :return:
        """
        mouse_x, mouse_y = event.scenePos().x(), event.scenePos().y()
        self.setPos(round_position(QPointF(mouse_x, mouse_y)))
        self.point_dragged.emit(self)

    def paint(self, painter, styles, widget=None):
        pen1 = QPen(Qt.SolidLine)
        pen1.setColor(self.color)
        painter.setPen(pen1)

        brush1 = QBrush(Qt.SolidPattern)
        brush1.setColor(self.color)
        painter.setBrush(brush1)

        painter.setRenderHint(QPainter.Antialiasing)  # 反锯齿
        painter.drawRoundedRect(self.boundingRect(), 10, 10)

    def hoverEnterEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        self.color = COLOR_HOVER_MID_POINT
        self.update()

    def hoverLeaveEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        self.color = COLOR_NORMAL
        self.update()

    def mousePressEvent(self, evt: QGraphicsSceneMouseEvent):
        print('clicked on port')
        if evt.button() == Qt.LeftButton:
            print("左键被按下")
            pos = (evt.scenePos().x(), evt.scenePos().y())
            # print(self.x(), type(self.x()), type(pos[1]))
            self.relative_pos = (pos[0] - self.x(), pos[1] - self.y())

        elif evt.button() == Qt.RightButton:
            print("左键被按下")
        elif evt.button() == Qt.MidButton:
            print("中间键被按下")

    def paintEvent(self, QPaintEvent):
        pen1 = QPen()
        pen1.setColor(QColor(166, 66, 250))
        painter = QPainter(self)
        painter.setPen(pen1)
        painter.begin(self)
        painter.drawRoundedRect(self.boundingRect(), 10, 10)  # 绘制函数
        painter.end()

    def mouseDoubleClickEvent(self, event: 'QGraphicsSceneMouseEvent') -> None:
        self.line.remove_mid_point(self)


class CustomPort(QGraphicsObject):
    port_clicked = pyqtSignal(QGraphicsObject)

    def __init__(self, port_id: int, content: object = None):
        super(CustomPort, self).__init__()
        self.setAcceptHoverEvents(True)  # 接受鼠标悬停事件
        self.relative_pos = (0, 0)
        self.color = COLOR_NORMAL
        self.id = port_id
        self.size = (10, 10)
        self.content = content
        self.connected_lines = []
        self.canvas = None

    def boundingRect(self):
        return QRectF(0, 0, self.size[0], self.size[1])

    @property
    def center_pos(self):
        return QPointF(self.x() + self.size[0] / 2, self.y() + self.size[1] / 2)

    def paint(self, painter, styles, widget=None):
        pen1 = QPen(Qt.SolidLine)
        pen1.setColor(QColor(128, 128, 128))
        painter.setPen(pen1)

        brush1 = QBrush(Qt.SolidPattern)
        brush1.setColor(self.color)
        painter.setBrush(brush1)

        painter.setRenderHint(QPainter.Antialiasing)  # 反锯齿
        painter.drawRoundedRect(self.boundingRect(), 10, 10)

    def hoverEnterEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        self.color = COLOR_HOVER_PORT

        self.update()

    def hoverLeaveEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        self.color = COLOR_NORMAL
        self.update()

    def mousePressEvent(self, evt: QGraphicsSceneMouseEvent):
        print('clicked on port')
        if evt.button() == Qt.LeftButton:
            print("左键被按下")
            pos = (evt.scenePos().x(), evt.scenePos().y())
            self.relative_pos = (pos[0] - self.x(), pos[1] - self.y())
            # print(self.relative_pos)
            self.port_clicked.emit(self)
        elif evt.button() == Qt.RightButton:
            print("左键被按下")
        elif evt.button() == Qt.MidButton:
            print("中间键被按下")

    def paintEvent(self, QPaintEvent):
        pen1 = QPen()
        pen1.setColor(QColor(166, 66, 250))
        painter = QPainter(self)
        painter.setPen(pen1)
        painter.begin(self)
        painter.drawRoundedRect(self.boundingRect(), 10, 10)  # 绘制函数
        painter.end()

    def get_pos(self) -> Tuple[int, int]:
        pos = self.pos()
        return pos.x(), pos.y()

    def on_delete(self):
        for line in self.connected_lines:
            line.on_delete()
        self.canvas.removeItem(self)

    def __repr__(self):
        return super(CustomPort, self).__repr__() + 'id = ' + str(self.id)


class CustomRect(QGraphicsItem):
    def __init__(self, node: 'Node' = None):
        super(CustomRect, self).__init__()
        # self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)  # 拖动
        self.setFlags(QGraphicsItem.ItemIsSelectable)  # 只有设置了可以选取才能被选中。
        self.setAcceptHoverEvents(True)  # 接受鼠标悬停事件
        self.relative_pos = (0, 0)
        self.color = COLOR_NORMAL
        self.node = node

    def boundingRect(self):
        return QRectF(0, 0, 200, 50)

    def paint(self, painter, styles, widget=None):
        pen1 = QPen(Qt.SolidLine)
        pen1.setColor(QColor(128, 128, 128))
        painter.setPen(pen1)

        brush1 = QBrush(Qt.SolidPattern)
        brush1.setColor(self.color)
        painter.setBrush(brush1)

        painter.setRenderHint(QPainter.Antialiasing)  # 反锯齿
        painter.drawRoundedRect(self.boundingRect(), 10, 10)

    def hoverEnterEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        self.color = COLOR_HOVER
        self.update()
        print('enter')

    def hoverLeaveEvent(self, event: 'QGraphicsSceneHoverEvent') -> None:
        self.color = COLOR_NORMAL
        if self.isSelected():
            self.color = COLOR_SELECTED
        print(self.color, self.color == COLOR_SELECTED)
        self.update()

    def mouseMoveEvent(self, event: QGraphicsSceneMouseEvent) -> None:
        self.setPos(round_position(
            QPointF(event.scenePos().x() - self.relative_pos[0],
                    event.scenePos().y() - self.relative_pos[1])))
        self.node.refresh_pos()

    def mousePressEvent(self, evt: QGraphicsSceneMouseEvent):
        print('鼠标按下')
        if evt.button() == Qt.LeftButton:
            print("左键被按下")
            pos = (evt.scenePos().x(), evt.scenePos().y())
            print(self.x(), type(self.x()), type(pos[1]))
            self.relative_pos = (pos[0] - self.x(), pos[1] - self.y())
            print(self.relative_pos)
            if not evt.modifiers() == Qt.ControlModifier:
                self.scene().signal_clear_selection.emit()
            self.scene().select_item(self)
            print(self.scene().selectedItems())
            self.color = COLOR_SELECTED
        elif evt.button() == Qt.RightButton:
            print("左键被按下")
        elif evt.button() == Qt.MidButton:
            print("中间键被按下")

    def paintEvent(self, paintEvent):
        pen1 = QPen()
        pen1.setColor(self.color)
        painter = QPainter(self)
        painter.setPen(pen1)
        painter.begin(self)
        painter.drawRoundedRect(self.boundingRect(), 10, 10)  # 绘制函数
        painter.end()

    def on_clear_selection(self):
        """
        当清除选择时触发的事件。
        :return:
        """
        self.color = COLOR_NORMAL
        self.scene().unselect_item(self)

    def on_delete(self):
        self.node.on_delete()
        pass
