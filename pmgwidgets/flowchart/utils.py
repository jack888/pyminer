from PyQt5.QtCore import QPointF


def round_position(point: QPointF, pixels=5):
    x, y = point.x(), point.y()
    x_cor, y_cor = round(x * 1.0 / pixels) * pixels, round(y * 1.0 / pixels) * pixels
    return QPointF(x_cor, y_cor)
